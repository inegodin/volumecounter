package niv.vc.rest.service;

import niv.vc.InvalidProfileException;
import niv.vc.ejb.VolumeCalculator;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import java.util.List;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

/**
 * Stateless implementation of {@link VolumeCalculatorService}
 */
@TransactionAttribute(NOT_SUPPORTED)
@Stateless
public class VolumeCalculatorServiceImpl implements VolumeCalculatorService {

    @EJB
    private VolumeCalculator volumeCalculator;

    /**
     * {@inheritDoc}
     */
    @Override
    public int calculate(List<Integer> profile) {
        try {
            return volumeCalculator.calculate(profile.stream().mapToInt(i -> i).toArray());
        } catch (InvalidProfileException exception) {
            throw new WebApplicationException(exception.getMessage(), exception, Response.Status.BAD_REQUEST);
        }
    }
}
