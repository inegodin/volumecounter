package niv.vc.rest.service;

import javax.ejb.Local;
import javax.validation.constraints.NotNull;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * REST interface to calculate the volume of water which remained after the rain.
 */
@Local
@Path("/volumeCalculator")
public interface VolumeCalculatorService {

    /**
     * Takes a list of integers as a surface profile and calculates the volume of water.
     *
     * @param profile of a surface as array
     * @return remained water volume
     */
    @POST
    @Produces(MediaType.TEXT_PLAIN)
    @Consumes(MediaType.APPLICATION_JSON)
    int calculate(@NotNull List<Integer> profile);
}
