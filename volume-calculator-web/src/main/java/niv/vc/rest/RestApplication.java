package niv.vc.rest;

import niv.vc.rest.service.VolumeCalculatorServiceImpl;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * Main REST application.
 */
@ApplicationPath("/rest")
public class RestApplication extends Application {
    @Override
    public Set<Class<?>> getClasses() {
        Set<Class<?>> classes = new HashSet<>();
        classes.add(VolumeCalculatorServiceImpl.class);
        return classes;
    }
}
