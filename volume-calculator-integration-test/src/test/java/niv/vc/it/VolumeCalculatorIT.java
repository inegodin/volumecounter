package niv.vc.it;

import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class VolumeCalculatorIT {

    private static final String URI = "http://localhost:8080/volume-calculator-web/rest/volumeCalculator";
    private static ResteasyClient client;
    private static ResteasyWebTarget target;

    @BeforeClass
    public static void setUpTests() {
        client = new ResteasyClientBuilder().build();
        target = client.target(URI);
    }

    @AfterClass
    public static void tearDownTests() {
        if (client != null) {
            client.close();
        }
    }

    @Test
    public void testPositive() {
        String input = "[1,1,0,1,1]";
        Response response = target.request().post(Entity.entity(input, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus(), is(200));
        Integer volume = response.readEntity(Integer.class);
        assertThat(volume, is(1));
        response.close();
    }

    @Test
    public void testUnsupportedMediaType() {
        Response response = target.request().post(Entity.entity(null, MediaType.APPLICATION_XML_TYPE));
        assertThat(response.getStatus(), is(415));
        response.close();
    }

    @Test
    public void testNegativeValue() {
        String input = "[1,1,0,-1,1]";
        Response response = target.request().post(Entity.entity(input, MediaType.APPLICATION_JSON_TYPE));
        assertThat(response.getStatus(), is(400));
        response.close();
    }

    @Test
    public void testMethodNotAllowed() {
        Response response = target.request().get();
        assertThat(response.getStatus(), is(405));
        response.close();
    }
}
