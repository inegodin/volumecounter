package niv.vc.ejb;

import niv.vc.InvalidProfileException;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class VolumeCalculatorTest {

    private static VolumeCalculator volumeCalculator = new VolumeCalculator();

    @Test
    public void calculateShouldReturn2() throws Exception {
        int volume = volumeCalculator.calculate(new int[]{3, 2, 4, 1, 2});
        assertThat(volume, is(2));
    }

    @Test
    public void calculateShouldReturn1() throws Exception {
        int volume = volumeCalculator.calculate(new int[]{3, 2, 2, 1, 2});
        assertThat(volume, is(1));
        volume = volumeCalculator.calculate(new int[]{1, 1, 1, 0, 1, 1});
        assertThat(volume, is(1));
        volume = volumeCalculator.calculate(new int[]{50, 100, 99, 100, 100, 50});
        assertThat(volume, is(1));
    }

    @Test
    public void calculateShouldReturn8() throws Exception {
        int volume = volumeCalculator.calculate(new int[]{4, 1, 1, 0, 2, 3});
        assertThat(volume, is(8));
    }

    @Test
    public void calculateShouldReturn12() throws Exception {
        int volume = volumeCalculator.calculate(new int[]{4, 1, 1, 0, 3, 0, 1, 3});
        assertThat(volume, is(12));
        volume = volumeCalculator.calculate(new int[]{6, 0, 0, 6});
        assertThat(volume, is(12));
    }

    @Test
    public void calculateShouldReturn0() throws Exception {
        int volume = volumeCalculator.calculate(new int[]{1, 1, 1, 1, 1, 1});
        assertThat(volume, is(0));
        volume = volumeCalculator.calculate(new int[]{1, 2, 3, 3, 2, 1});
        assertThat(volume, is(0));
        volume = volumeCalculator.calculate(new int[]{1});
        assertThat(volume, is(0));
    }

    @Test(expected = InvalidProfileException.class)
    public void calculateShouldThrowException() throws Exception {
        volumeCalculator.calculate(new int[]{2, 3, -1, 0, 9, 1});
    }

}