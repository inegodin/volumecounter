package niv.vc.ejb;

import niv.vc.InvalidProfileException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import java.util.Arrays;

import static javax.ejb.TransactionAttributeType.NOT_SUPPORTED;

/**
 * Bean to calculate the volume of water which remained after the rain.
 */
@Stateless
@LocalBean
@TransactionAttribute(NOT_SUPPORTED)
public class VolumeCalculator {

    /**
     * Takes an array as a surface profile and calculates the volume of water.
     *
     * @param profile of a surface as array
     * @return remained water volume
     */
    public int calculate(int[] profile) {
        validateProfile(profile);
        return calculateVolume(profile);
    }

    private int calculateVolume(int[] profile) {
        int leftHighest = 0, rightHighest = 0, left = 0, volume = 0;
        int right = profile.length - 1;
        while (left < right) {
            if (profile[right] > rightHighest) {
                rightHighest = profile[right];
            }
            if (profile[left] > leftHighest) {
                leftHighest = profile[left];
            }
            if (leftHighest >= rightHighest) {
                volume += rightHighest - profile[right];
                right--;
            } else {
                volume += leftHighest - profile[left];
                left++;
            }
        }
        return volume;
    }

    private void validateProfile(int[] profile) {
        if (Arrays.stream(profile).filter(i -> i < 0).count() > 0) {
            throw new InvalidProfileException("Profile of a surface cannot be setup using negative values");
        }
    }
}
