package niv.vc;

import javax.ejb.ApplicationException;
import java.io.Serializable;

/**
 * Exception to indicate that a surface was setup using invalid profile values.
 */
@ApplicationException
public class InvalidProfileException extends RuntimeException implements Serializable {

    public InvalidProfileException(String message) {
        super(message);
    }
}
